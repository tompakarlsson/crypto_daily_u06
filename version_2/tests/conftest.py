from sqlalchemy.pool import StaticPool
from sqlmodel import create_engine, Session, SQLModel
import pytest

# @pytest.fixture()
# def session():
#     db_url = 'test'
#     engine = create_engine(url=db_url, echo=False)
#     SQLModel.metadata.create_all(engine)
#     return Session(engine)
from src.crypto import get_crypto_results_by_time_span
from src.models import Coin


@pytest.fixture(name='session')
def session_fixture():
    engine = create_engine(
        "sqlite://", connect_args={'check_same_thread': False}, poolclass=StaticPool
    )
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


@pytest.fixture(name='fixture_crypto_results_data')
def fixture_export_crypto_results_data():
    coins = [Coin(symbol='btc',
                  current_price=29471,
                  last_updated='2022-05-21T21:47:40.645Z',
                  price_change_percentage_1h_in_currency=-0.029913,
                  price_change_percentage_24h_in_currency=0.785263,
                  price_change_percentage_7d_in_currency=0.546027,
                  name='bitcoin')]
    return coins
