import pytest
from sqlmodel import Session
from sqlmodel.sql.expression import Select, SelectOfScalar

from src.models import Coin
from src.crud import get_coins, list_available_coins


# Workaround for SAWarcning in SQLModel (discussed here: https://github.com/tiangolo/sqlmodel/issues/189)
SelectOfScalar.inherit_cache = True
Select.inherit_cache = True


def test_get_coins(session: Session):
    # GIVEN
    coin_name = ('bitcoin',)
    session.add(Coin(symbol='btc',
                     current_price=29471,
                     last_updated='2022-05-21T21:47:40.645Z',
                     price_change_percentage_1h_in_currency=-0.029913,
                     price_change_percentage_24h_in_currency=0.785263,
                     price_change_percentage_7d_in_currency=0.546027,
                     name=coin_name[0]))

    # WHEN
    coins = get_coins(session=session, coin_name=coin_name)

    # THEN
    assert isinstance(coins,list)


def test_get_coins_valueerror(session: Session):
    # GIVEN
    coin_name = ('bitcoin',)
    session.add(Coin(symbol='btc',
                     current_price=29471,
                     last_updated='2022-05-21T21:47:40.645Z',
                     price_change_percentage_1h_in_currency=-0.029913,
                     price_change_percentage_24h_in_currency=0.785263,
                     price_change_percentage_7d_in_currency=0.546027,
                     name='ethereum'))

    # WHEN
    with pytest.raises(ValueError):
        coins = get_coins(session=session, coin_name=coin_name)

    # THEN
    assert ValueError


def test_list_available_coins(session: Session, capfd):
    # GIVEN
    coin_name = ('bitcoin',)
    session.add(Coin(symbol='btc',
                     current_price=29471,
                     last_updated='2022-05-21T21:47:40.645Z',
                     price_change_percentage_1h_in_currency=-0.029913,
                     price_change_percentage_24h_in_currency=0.785263,
                     price_change_percentage_7d_in_currency=0.546027,
                     name=coin_name[0]))

    # WHEN
    list_available_coins(session=session)

    # THEN
    out, err = capfd.readouterr()
    assert out == "The following symbols can be used:\nsymbol    name\n--------  -------\nbtc       bitcoin\n"
