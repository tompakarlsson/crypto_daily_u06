from click.testing import CliRunner

import src.crypto
from cryptodaily.src.crypto import get_crypto_results_by_time_span, export_crypto_results, read_coin, export_csv

# def test_read_coin(mocker):
#     mocker.patch('cryptodaily.src.crypto.load_config', return_value={'crypto_currency': ['bitcoin'], 'email_address': 'tomas.karlsson@chasacademy.se'})
#
#     runner = CliRunner()
#     result = runner.invoke(read_coin)
#     assert result.exit_code == 0
#     assert '''=======================================  ========================
#     name                                     bitcoin
#     current_price                            29471
#     last_updated                             2022-05-21T21:47:40.645Z
#     price_change_percentage_7d_in_currency   0.5460272035273179
#     price_change_percentage_24h_in_currency  0.7852629661694194
#     price_change_percentage_1h_in_currency   -0.02991295241692709
#     =======================================  ========================
#     ''' in result.output
from src.models import Coin


def test_get_crypto_results_by_time_span_full(fixture_crypto_results_data):
    # GIVEN
    coins = fixture_crypto_results_data

    # WHEN
    crypto_results = get_crypto_results_by_time_span(coins=coins, time_span='')

    # THEN
    assert isinstance(crypto_results, list)


def test_get_crypto_results_by_time_span_hour(fixture_crypto_results_data):
    # GIVEN
    coins = fixture_crypto_results_data

    # WHEN
    crypto_results = get_crypto_results_by_time_span(coins=coins, time_span='h')

    # THEN
    assert isinstance(crypto_results, list)


def test_get_crypto_results_by_time_span_day(fixture_crypto_results_data):
    # GIVEN
    coins = fixture_crypto_results_data

    # WHEN
    crypto_results = get_crypto_results_by_time_span(coins=coins, time_span='d')

    # THEN
    assert isinstance(crypto_results, list)


def test_get_crypto_results_by_time_span_week(fixture_crypto_results_data):
    # GIVEN
    coins = fixture_crypto_results_data

    # WHEN
    crypto_results = get_crypto_results_by_time_span(coins=coins, time_span='w')

    # THEN
    assert isinstance(crypto_results, list)


def test_export_crypto_results_stdout(capfd, fixture_crypto_results_data):
    # GIVEN
    crypto_results = get_crypto_results_by_time_span(coins=fixture_crypto_results_data, time_span='')

    # WHEN
    export_crypto_results(crypto_results=crypto_results, export='stdout')

    # THEN
    out, err = capfd.readouterr()
    assert 'Price change (%) within the last hour' in out


def test_export_crypto_results_email(capfd, fixture_crypto_results_data):
    # GIVEN
    crypto_results = get_crypto_results_by_time_span(coins=fixture_crypto_results_data, time_span='')

    # WHEN
    export_crypto_results(crypto_results=crypto_results, export='email')

    # THEN
    out, err = capfd.readouterr()
    assert out in ['Report sent by email to tomas.karlsson@chasacademy.se\n',
                   'Connection to mailserver refused. Please make sure mailhog is running in docker.\n']


def test_export_crypto_results_csv(capfd, tmpdir, fixture_crypto_results_data):
    # GIVEN
    crypto_results = get_crypto_results_by_time_span(coins=fixture_crypto_results_data, time_span='')

    # WHEN
    file = tmpdir.join('csvexport.csv')
    export_csv(filepath=file.strpath, content=crypto_results)

    # THEN
    assert 'bitcoin' in file.read()
