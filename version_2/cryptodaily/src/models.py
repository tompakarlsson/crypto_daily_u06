import configparser
from pathlib import Path

from pydantic import BaseModel
from sqlmodel import SQLModel, Field

THIS_PACKAGE = Path(__file__).parent.parent
CONFIG = f"{THIS_PACKAGE}/config/config.ini"

config = configparser.ConfigParser()
config.read(CONFIG)
fiat_currency: str = config['general']['fiat currency']


class CoinBase(BaseModel):
    """
    Pydantic model representing the base model of Coin
    """
    name: str = Field(..., alias='Name')
    current_price: str = Field(..., alias=f'Current price in {fiat_currency}')
    last_updated: str = Field(..., alias='Last updated')

    class Config:
        allow_population_by_field_name = True


class CoinHour(CoinBase):
    """
    Pydantic model that adds price_change_percentage_1h_in_currency to CoinBase
    """
    price_change_percentage_1h_in_currency: str = Field(..., alias='Price change (%) within the last hour')


class CoinDay(CoinBase):
    """
    Pydantic model that adds price_change_percentage_24h_in_currency to CoinBase
    """
    price_change_percentage_24h_in_currency: str = Field(..., alias='Price change (%) within the last 24 hours')


class CoinWeek(CoinBase):
    """
    Pydantic model that adds price_change_percentage_7d_in_currency to CoinBase
    """
    price_change_percentage_7d_in_currency: str = Field(..., alias='Price change (%) within the last seven days')


class CoinHourDayWeek(CoinHour, CoinDay, CoinWeek):
    """
    Pydantic model that inherits CoinHour, CoinDay and CoinWeek
    """
    pass


class Coin(CoinHourDayWeek, SQLModel, table=True):
    """
    Pydantic model representing "coin" table in database
    Inherits properties of CoinHourDayWeek
    """
    # __table_args__ = {'extend_existing': True}
    coin_id: int | None = Field(default=None, primary_key=True)
    symbol: str = Field(index=True)
