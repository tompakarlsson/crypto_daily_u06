import configparser
import smtplib
from typing import Any
from pathlib import Path

import click

from pandas import DataFrame

from sqlmodel import Session
from sqlmodel.sql.expression import SelectOfScalar, Select

from tabulate import tabulate

from src.database import engine
from src.models import Coin, CoinDay, CoinHour, CoinWeek, CoinHourDayWeek
from src.crud import get_coins, list_available_coins


THIS_PACKAGE = Path(__file__).parent.parent
CONFIG = f"{THIS_PACKAGE}/config/config.ini"


# Workaround for SAWarcning in SQLModel (discussed here: https://github.com/tiangolo/sqlmodel/issues/189)
SelectOfScalar.inherit_cache = True
Select.inherit_cache = True


def load_config():
    """
    Load configuration variables from a config.ini file
    """
    config = configparser.ConfigParser()
    config.read(CONFIG)
    crypto_currency: list = config['general']['crypto currency'].replace(' ', '').split(',')
    email_address: str = config['alarm']['email address']
    return {'crypto_currency': crypto_currency, 'email_address': email_address}


def get_crypto_results_by_time_span(coins: list[Coin], time_span: str | None) -> list[dict[str, Any]]:
    """
    Filters instance of Coin by using time span specific Coin models
    Then translating the time span specific Coin models into dictionaries by alias
    """
    crypto_results = []
    for coin in coins:
        coin_dict = coin.dict()
        match time_span:
            case 'hour' | 'h':
                crypto_results.append(CoinHour(**coin_dict).dict(by_alias=True))
            case 'day' | 'd':
                crypto_results.append(CoinDay(**coin_dict).dict(by_alias=True))
            case 'week' | 'w':
                crypto_results.append(CoinWeek(**coin_dict).dict(by_alias=True))
            case 'full' | _:
                crypto_results.append(CoinHourDayWeek(**coin_dict).dict(by_alias=True))
    return crypto_results


def export_crypto_results(crypto_results: list[dict[str, Any]], export: str):
    """
    Export a list of dicts to .csv, email or stdout
    """
    match export:
        case 'csv':
            export_csv(filepath='csvexport.csv', content=crypto_results)  # pragma: no cover
        case 'email':
            mailhog(crypto_results)
        case 'stdout' | _:
            click.echo(f"\n{tabulate(crypto_results, showindex=False, headers='keys')}")


def export_csv(filepath: str, content: list[dict[str, Any]]):
    """
    Writes a .csv file with filename specified in parameter <filepath>
    """
    csv_export = DataFrame(content)
    csv_export.to_csv(path_or_buf=filepath, mode='w', index=False)


def mailhog(content: list[dict[str, Any]]):
    """
    Send an email with a price change percentage report for one or more crypto currencies
    """
    to_addrs = load_config()['email_address']
    from_addr = 'mailhog@crypto.app'
    subject = 'Report from Crypto Daily CLI'

    email_body = DataFrame(content)

    msg = f'''From: {from_addr}\r\nSubject: {subject}\r\nTo: {to_addrs}\r\n\r\n
    \n{tabulate(email_body, headers='keys', tablefmt='rst', showindex=False)}
    '''
    try:
        server = smtplib.SMTP("localhost:1025")
        server.sendmail(from_addr=from_addr, to_addrs=to_addrs, msg=msg)
        click.echo(f'Report sent by email to {to_addrs}')
    except ConnectionRefusedError:
        click.echo('Connection to mailserver refused. Please make sure mailhog is running in docker.')


@click.command()
@click.option('--name', '-n', type=str, multiple=True,
              help='Specify which cryptocurrency to fetch from database. Can be used multiple times.')
@click.option('--time-span', '-t',
              type=click.Choice(['hour', 'day', 'week', 'full', 'h', 'd', 'w', 'f'], case_sensitive=False),
              help='Specify price change time span by 1 hour(hour), 24 hours(day), 7 days(week) or all three(full)')
@click.option('--export', '-e', type=click.Choice(['stdout', 'csv', 'email'], case_sensitive=False),
              help='Specify if output is sent to screen(stdout), csv or email')
@click.option('--list-all-coins', '-l', 'listcoins',
              is_flag=True, show_default=True, default=False, help="List all coins in database")
def read_coin(name: tuple[str] | None, time_span: str | None, export: str | None, listcoins: bool = False) -> list[Coin] | None:
    """
    \b
    Get price change percentage for a crypto currency.
    All data is downloaded from the Crypto Daily daemon every minute.
    Configuration can be made in .config/config.ini
    \f
    :param listcoins: Bool
    :param export: str | None
    :param time_span: str | None
    :param name: tuple[str] | None
    :return: list[Coin]
    """

    if listcoins:  # pragma: no cover
        with Session(engine) as session:
            list_available_coins(session=session)
        return

    name = name or load_config()['crypto_currency']  # pragma: no cover
    with Session(engine) as session:  # pragma: no cover
        try:
            coins: list[Coin] = get_coins(session=session, coin_name=name)
        except ValueError:
            list_available_coins(session=session)
            return

    crypto_results: list[dict[str, Any]] = get_crypto_results_by_time_span(coins=coins, time_span=time_span)  # pragma: no cover
    export_crypto_results(crypto_results=crypto_results, export=export)  # pragma: no cover
