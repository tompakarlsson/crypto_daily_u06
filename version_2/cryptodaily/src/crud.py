import click
from sqlmodel import Session, select
from tabulate import tabulate

from src.models import Coin


def get_coins(session: Session, coin_name: tuple[str]) -> list[Coin]:
    """
    CRUD function that queries the database for rows in table coin that corresponds with a given coin_name
    Returns a list of one or more Coin models
    """
    statement = select(Coin).where(Coin.name.in_(coin_name))
    results = session.exec(statement)
    coins = results.all()
    if not coins:
        raise ValueError('no such coin')
    return coins


def list_available_coins(session: Session):
    """
    CRUD function that queries the database for all fields in column symbol and name
    Sends tabulated view of data to stdout
    """
    statement = select(Coin.symbol, Coin.name)
    results = session.exec(statement)
    click.echo('\nThe following names can be used:')
    click.echo(tabulate(results.all(), headers=['symbol', 'name']))