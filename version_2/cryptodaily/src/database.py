"""Holds the database information"""
from sqlmodel import create_engine
from pathlib import Path
THIS_PACKAGE = Path(__file__).parent.parent
SQLITE_FILE_NAME = f"{THIS_PACKAGE}/db/db_coins.db"

sqlite_url = f'sqlite:///{SQLITE_FILE_NAME}'
engine = create_engine(url=sqlite_url)