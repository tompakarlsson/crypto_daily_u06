from setuptools import find_packages, setup

setup(
    name='cryptodaily',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'sqlmodel',
        'tabulate',
        'pandas',
    ],
    entry_points={
        'console_scripts': [
            'crypto = src.crypto:read_coin',
        ],
    },
)