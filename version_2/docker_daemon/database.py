"""Holds the database information"""
from pathlib import Path

from sqlmodel import create_engine, SQLModel

import os

import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger: logging.Logger = logging.getLogger('Docker daemon')

if os.getenv("DOCKER_DAEMON_DB_PATH"):
    SQLITE_FILE_NAME = os.getenv("DOCKER_DAEMON_DB_PATH")
else:
    THIS_PACKAGE = Path(__file__).parent.parent
    SQLITE_FILE_NAME = f"{THIS_PACKAGE}/cryptodaily/db/db_coins.db"

sqlite_url = f'sqlite:///{SQLITE_FILE_NAME}'
logging.warning(sqlite_url)
engine = create_engine(url=sqlite_url)


# """Holds the database information"""
# from pathlib import Path
#
# from sqlmodel import create_engine, SQLModel
#
# SQLITE_FILE_NAME = Path('../cryptodaily/db/db_coins.db')
# sqlite_url = f'sqlite:///{SQLITE_FILE_NAME}'
#
# engine = create_engine(sqlite_url, echo=False)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)
