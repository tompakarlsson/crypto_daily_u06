"""docker daemon for monitoring cryptocurrencies and sending alarms"""
from concurrent.futures import ThreadPoolExecutor
import configparser
import logging
from pathlib import Path
import time

import requests

from sqlmodel import Session
from sqlmodel.sql.expression import SelectOfScalar, Select

from watchdog.events import PatternMatchingEventHandler
from watchdog.observers import Observer

from crud import update_db, read_coin
from database import engine
from mailhogger import mailhog
from models import Config

import os

# Workaround for SAWarcning in SQLModel (discussed here: https://github.com/tiangolo/sqlmodel/issues/189)
SelectOfScalar.inherit_cache = True
Select.inherit_cache = True

# Set basic configuration for logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger: logging.Logger = logging.getLogger('Docker daemon')

if os.getenv("DOCKER_DAEMON_CONF_PATH"):
    CONFIG = os.getenv("DOCKER_DAEMON_CONF_PATH")
else:
    THIS_PACKAGE = Path(__file__).parent.parent
    CONFIG = f"{THIS_PACKAGE}/cryptodaily/config/"
print(CONFIG)


class OnMyWatch:
    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, path=CONFIG, recursive=False)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except KeyboardInterrupt:
            self.observer.stop()
        self.observer.join()


class Handler(PatternMatchingEventHandler):
    def __init__(self):
        """
        Set the patterns for PatternMatchingEventHandler
        """
        PatternMatchingEventHandler.__init__(self,
                                             patterns=['config.ini'],
                                             ignore_directories=True,
                                             case_sensitive=False)

    def on_modified(self, event):
        """
        Describe actions if a monitored file is modified
        :param event:
        :return:
        """
        logger.info('Changes detected in config')
        global config
        config = load_config()


def load_config(config_path: Path = Path(f'{CONFIG}config.ini')) -> Config:
    """
    Load configuration variables from config file and store them as an instance of Config
    :param config_path: Path
    :return: Config
    """
    parser = configparser.ConfigParser()
    parser.read(config_path)

    config = Config(
        fiat_currency=parser['general']['fiat currency'],
        crypto_currency=parser['general']['crypto currency'].replace(" ", "").replace(",", "%2C%20"),
        alarm_active=parser['alarm'].getboolean('active'),
        email_address=parser['alarm']['email address'],
        alarm_currency=parser['alarm']['currency'],
        lower_threshold=parser['alarm'].getfloat('lower threshold'),
        upper_threshold=parser['alarm'].getfloat('upper threshold'))

    logger.info(f'Monitored cryptos: {config.crypto_currency.replace("%2C%20", ", ")}')
    logger.info(f'Alarm set to {config.alarm_active} for {config.alarm_currency.replace("%2C%20", ", ")}: '
                f'lower threshold {config.lower_threshold}, '
                f'upper threshold {config.upper_threshold}')

    return config


def fetch_data():
    """
    Fetches data from CoinGecko API and calls crud function update_db to update the database
    :return:
    """
    while True:
        url: str = f'https://api.coingecko.com/api/v3/coins/markets?vs_currency={config.fiat_currency}&ids={config.crypto_currency}&order=market_cap_desc&sparkline=false&price_change_percentage=1h%2C24h%2C7d'
        response = requests.get(url).json()

        logger.info('Data pulled from CoinGecko API')

        with Session(engine) as session:
            update_db(session=session, indata=response)
            logger.info('Data written to database')

        if config.alarm_active:
            check_if_alarm()

        time.sleep(20)


def check_if_alarm():
    """
    Checks if an alarm is to be sent by checking last hours price change in percent against lower and upper threshold
    set in config.
    :return:
    """
    with Session(engine) as session:
        price_change = read_coin(session=session, symbol=config.alarm_currency.replace(' ', '').split(','))
        for coin in price_change:
            if price_change[coin] <= config.lower_threshold:
                logger.info(f'{coin} is dropping more than {config.lower_threshold}')
                mailhog(currency=[coin, price_change[coin]], email_address=config.email_address)
            elif price_change[coin] >= config.upper_threshold:
                logger.info(f'{coin} is increasing more than {config.lower_threshold}')
                mailhog(currency=[coin, price_change[coin]], email_address=config.email_address)
        logger.info(f'Alarm sent to {config.email_address}')


if __name__ == '__main__':
    config = load_config()

    watch = OnMyWatch()

    with ThreadPoolExecutor(max_workers=2) as tpe:
        futures = [tpe.submit(fetch_data), tpe.submit(watch.run)]
