import smtplib


def mailhog(currency: list, email_address: str):
    """
    Send an email with an alarm message if an alarm is triggered.
    :param currency: list
    :param email_address: str
    :return:
    """

    from_addr = 'mailhog@crypto.app'
    to_addr = email_address
    subject = f'Alarm: {currency[0]}'

    msg = f'''From: {from_addr}\r\nSubject: {subject}\r\nTo: {to_addr}\r\n\r\n 
    ALARM!
    You have set an alarm for {currency[0]}.
    Its recent price change is {currency[1]}.
    '''
    server = smtplib.SMTP("localhost:1025")
    server.sendmail(from_addr, to_addr, msg)