from typing import Optional

from pydantic import condecimal
from sqlmodel import Field, SQLModel


class Coin(SQLModel, table=True):
    """
    Data model representing "coin" table in database
    """
    coin_id: Optional[int] = Field(default=None, primary_key=True)
    name: str = Field(index=True)
    symbol: str
    current_price: condecimal(max_digits=20, decimal_places=10) = Field(default=0)
    price_change_percentage_1h_in_currency: condecimal(max_digits=23, decimal_places=20) = Field(default=0)
    price_change_percentage_24h_in_currency: condecimal(max_digits=23, decimal_places=20) = Field(default=0)
    price_change_percentage_7d_in_currency: condecimal(max_digits=23, decimal_places=20) = Field(default=0)
    last_updated: str


class Config(SQLModel):
    fiat_currency: str
    crypto_currency: str
    alarm_active: bool
    email_address: str
    alarm_currency: str
    lower_threshold: float
    upper_threshold: float
