from decimal import Decimal

from sqlmodel import select, Session

# from database import create_db_and_tables
from models import Coin


def update_db(session: Session, indata: list[dict]):
    """
    Updates table coin in database with data fetched from CoinGecko API
    """
    # create_db_and_tables()  # remove comment to create a new database
    for row in indata:
        statement = select(Coin).where(Coin.name == row['id'])
        results = session.exec(statement)
        coin: Coin = results.one()
        coin.name = row['id']
        coin.symbol = row['symbol']
        coin.current_price = row['current_price']
        coin.price_change_percentage_1h_in_currency = row['price_change_percentage_1h_in_currency']
        coin.price_change_percentage_24h_in_currency = row['price_change_percentage_24h_in_currency']
        coin.price_change_percentage_7d_in_currency = row['price_change_percentage_7d_in_currency']
        coin.last_updated = row['last_updated']

        # coin = Coin(name=row['id'],  # remove comments to create a data for a new database
        #             symbol=row['symbol'],
        #             price_usd=row['current_price'],
        #             change_1h_percent=row['price_change_percentage_1h_in_currency'],
        #             change_24h_percent=row['price_change_percentage_24h_in_currency'],
        #             change_7d_percent=row['price_change_percentage_7d_in_currency'],
        #             last_updated=row['last_updated'])
        session.add(coin)
    session.commit()


def read_coin(session: Session, symbol: list[str]):
    """
    Read column <price_change_percentage_1h_in_currency>
    Used by function <check_if_alarm> to check values against thresholds given in config.ini
    """
    coins: list = []
    for each_coin in symbol:
        statement = select(Coin.price_change_percentage_1h_in_currency).where(Coin.name == each_coin)
        results = session.exec(statement)
        coin: Decimal = results.one()
        coins.append(coin)
    return dict(zip(symbol, coins))
