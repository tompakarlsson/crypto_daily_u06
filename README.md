# ![](https://i.imgur.com/vV68Gs6.png)

>## u06 Soloproject DOE21, Chas Academy
Develop an application to get the top10 crypto coins from API downloaded to a Sqlite database, select the data wanted and send a report by MailHog. The project is developed in Python.

A solo project with 4 moment. 
- Planning, 1 week
- sprint 1, 3 weeks
- handover, 1 week. 
- sprint 2, 3 veeks

After working solo with this project "It´s all about crypto now!" I hand over this project to one of my school team member. This person gets a handover by me and then continue this application according to the planning. Find the planning document down below.
## Files & folders in this repository
- README.md
- .gitignore
- .gitlab.yml
- Dockerfile
- docker-compose.yaml
- maildir/
- requirements.txt
- src/
    - aras_all_in_one_.py
    - user_choice_api.py

## To-do on handover
- "Fork" this repository.
- Read this README.md
- Do the installation needed to run the application. Please see "Installation" dow below.
- Continue the work according to the plan for sprint 1 in the down below document:
https://docs.google.com/document/d/16sWO6KkhfXqQ3EMUyhdAViWXFmrrSJecTuYik7u4_lA/edit

## Repository
https://gitlab.com/SaraPetre/crypto_daily_u06

## Planning
https://docs.google.com/document/d/16sWO6KkhfXqQ3EMUyhdAViWXFmrrSJecTuYik7u4_lA/edit

### Technologies Used

<img align="left" alt="Docker" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/docker/docker.png" />

<img align="left" alt="Python" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png" />

<img class="logo"  src="images/sqlite370_banner.gif" alt="SQLite" width="26px" border="0" />

<img src="images/hog.png" height="20" alt="MailHog"/>
<br />
<br />


## Visuals- This is the report that ends up in MailHog

![](https://i.imgur.com/aEorryb.png)

## Installation
### Install the requirement with the following command in your terminal
>pip install -r requirements.txt
### Install browser for visual image of sqlite-Optional
https://sqlitebrowser.org/dl/
### Install Docker desktop - Needed
https://www.docker.com/products/docker-desktop/
### Start MailHog by Docker. Run in terminal:
>docker run -d -p 1025:1025 -p 8025:8025 mailhog/mailhog

##### OR

### Run by starting from the Docker image, and Docker compose -file. You need to be in the same folder as the Docker and Docker Compose -files.

### If it is the first time
> docker-compose up -d --build
### Then you can use:
> docker-compose up -d

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.
## Test by pipeline CI/CD in GitLab
There are code quality tests set up in the file:
> docker-compose.yaml

The tests done is:
> - pycodestyle
> - pylint 
> - flake8


# VERSION 2
Version 2 consists of two parts:
 - A docker daemon that uses an API to fetch data every 20th second. It writes collected data to a SQLite database. It also monitores a config file for changes and reloads the config when it has detected a file change. If alarm is set to True in config then it will send an alarm by email if price change the last hour is more or less than the upper or lower threshold set in config 
 - A CLI app that uses the same database to output data to stdout, csv or email

To use the (docker) daemon:
 - create and activate a virtual environment of your choice with __Python 3.10__
 - run `pip install -r requirements.txt`
 - to test the daemon locally:
   - set up mailhog in docker
   - run `python version_2/docker_daemon/main.py`
 - to set an alarm:
   - open __version_2/cryptodaily/config/config.ini__
   - section [alarm]:
     - set __active__ to _True_
     - change __email address__ to your email address (just in theory. All emails will be sent to mailhog anyway)
     - choose which currency/currencies you would want an alarm for
     - set the lower and upper threshold

To use the CLI app:
 - create and/or activate a virtual environment of your choice wit __Python 3.10__
 - cd into __version_2/cryptodaily__
 - run `pip install --editable .`
 - `crypto --help` will show you how the app is used
   - Examples:
     - `crypto --name bitcoin --timespan hour --export stdout` will show price change of Bitcoin on screen
     - `crypto` will fall back to defaults and show all crypto coins with price change for 1 hour, 24 hours and 7 days
 - To change what crypto coins will show by default you can edit __version_2/cryptodaily/config/config.ini__
 - In previously mentioned config file you can also change what fiat currency you want to use